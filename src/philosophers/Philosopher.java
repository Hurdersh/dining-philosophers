package philosophers;
import java.util.Random;

import gui.Gui;
import synchronization.Chopstick;
/*
 * @author giordano colli
 * @value theer are many ways to solve problem of synchronization between philosopher
 * i'll use the most optimized, 2 types of Philopher left-handed and right-handed  
 */
public abstract class Philosopher implements Runnable{
	//n is number of philosopher(used for index of chopstick)
	protected Integer n;
	protected Chopstick chopsticks[];
	//gui is used to update the graphics
	Gui gui;
	public Philosopher(Integer n, Chopstick chopsticks[],Gui gui) {
		this.n = n;
		this.chopsticks = chopsticks;
		this.gui = gui;
	}
	//this method change in 2 philosophers subclasses
	public abstract void eat();
	//used to start threads
	@Override
	public void run() {
		while(true) {
			eat();
			think();
		}
	}
	//this method wait for x numbers of second from 0.1 to 10 seconds
	public void think() {
		Random r = new Random();
		Integer x = (r.nextInt(100))+1;
		//sleep random number of seconds
		try {Thread.sleep(x*100);} catch (InterruptedException e) {e.printStackTrace();}
	}
}
