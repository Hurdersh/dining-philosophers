package philosophers;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
/**
 *@author giordano colli
 *@value this class is used to store information about every philosophes 
 */
public class Info{
	//array of times but i called dates(i'm really dude)
	private static String dates[] = {null,null,null,null,null};
	//counter of up's (chopsticks taken)
	private static Integer up[] = {0,0,0,0,0};
	//counter of up's (chopsticks leaved)
	private static Integer down[] = {0,0,0,0,0};

	//standard getter methods :), if you don't know this shit you must torture yourself studying java 
	public static String getDate(Integer n){
		return dates[n];
	}

	public static Integer getUp(Integer n){
		return up[n];
	}
	
	public static Integer getDown(Integer n){
		return down[n];
	}
	//increment up is called from every thread,it starts 
	public static void IncrementUp(Integer n){
		up[n]+=1;
	}
	//increment down is called every time 1 thread leaves chopsticks
	//set the date of the last eat finish
	public static void IncrementDown(Integer n){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		dates[n] = String.valueOf(dtf.format(now));
		down[n]+=1;
	}
}
