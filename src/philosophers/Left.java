package philosophers;
import gui.Gui;
import synchronization.Chopstick;
/*
 * @author giordano colli
 * @value this class is used to differentiate left-handed philosopher from right-handed
 */

public class Left extends Philosopher{

	public Left(Integer n,Chopstick chopsticks[],Gui gui) {
		super(n,chopsticks,gui);
	}

	@Override
	public void eat() {
		//trying to take left chopstick
		chopsticks[n].take();
		//trying to take right chopstick
		chopsticks[(n+1)%5].take();
		//set status of me as true(eating)
		gui.setStatus(n, true);
		Info.IncrementUp(n);
		gui.repaint();
		System.out.println("philosopher number " + n + " is eating");
		this.think(); //only for wait, philosopher is eating but i'm use the same method for sleep
		System.out.println("philosopher number "+ n + " leave");
		//set status of me as false(thinking)
		gui.setStatus(n, false);
		Info.IncrementDown(n);
		gui.repaint();
		//leave every 2 the chopsticks
		chopsticks[n].leave();
		chopsticks[(n+1)%5].leave();
		//here,in base of the original problem should be a function think() but isn't really relevant 
		//'cause philosopher will wait until the other will leave chopsticks
	}

}
