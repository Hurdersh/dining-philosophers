package philosophers;
import gui.Gui;
import synchronization.Chopstick;
/*
 * @author giordano colli
 * @value this class is used to differentiate right-handed philosopher from left-handed
 */
//this class is a thread and implements personal
public class Right extends Philosopher{
	
	public Right(Integer n,Chopstick chopsticks[],Gui gui) {
		super(n,chopsticks,gui);
	}
	
	@Override
	public void eat(){
		//trying to take right chopstick
		chopsticks[(n+1)%5].take();
		//trying to take left chopstick
		chopsticks[n].take();
		//set status of me as true(eating)
		gui.setStatus(n, true);
		Info.IncrementUp(n);
		gui.repaint();
		System.out.println("philosopher number " + n + " is eating");
		this.think(); //only for wait, philosopher is eating but i'm use the same method for sleep
		System.out.println("philosopher number "+ n + " leave");
		//set status of me as true(thinking)
		gui.setStatus(n, false);
		Info.IncrementDown(n);
		gui.repaint();
		//leave every 2 chopsticks
		chopsticks[(n+1)%5].leave();
		chopsticks[n].leave();
		//here,in base of the original problem should be a function think() but isn't really relevant 
		//'cause philosopher will wait until the other will leave chopsticks
	}

}
