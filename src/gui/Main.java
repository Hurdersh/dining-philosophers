package gui;
import synchronization.Chopstick;
import philosophers.*;
/*
 * @author giordano colli
 * @value this class is used to start program and gui,and for creating and instantiate threads 
 */
public class Main {

	public static void main(String[] args) {
		//initialization of chopstick array, all chopsticks are usable=true
		Chopstick chopsticks[] = new Chopstick[5];
		for(int i = 0;i<5;i++) {
			chopsticks[i] = new Chopstick(true);
		}
		
		//initialization of GUI interface with all green ovals
		Gui gui = new Gui();
		
		//initialization of philosophers array
		Thread threads[] = new Thread[5];
		for(int i = 0;i<5;i++) {
			//if index is odd thread will contains Left class instance
			if(i%2 == 1) {
				threads[i] = new Thread(new Left(i,chopsticks,gui));
			}
			//if index is even thread will contains Right class instance
			else {
				threads[i] = new Thread(new Right(i,chopsticks,gui));
			}
		}
		
		//start every thread
		for(int i = 0;i<5;i++) {
			threads[i].start();
		}
	}
}
