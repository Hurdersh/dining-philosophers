package gui;
import javax.swing.*;

import philosophers.Info;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.*;
/*
 * @author giordano colli
 * @value this class is used for GUI interface
 */

public class Gui extends JFrame{
	//this array represent status of philosophers, true if they are eating and false if they are thinking
	//this array can be moved into info class and implemented with getter
	private Boolean status[]  = {false,false,false,false,false};
	private BufferedImage[] images = new BufferedImage[5]; 
	public Gui() {
		//various information about frame
		setTitle("Philosophers");
		setSize(1000,700);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			//buffer all the five images for not recharges when they will wrote into gui
			//improve this with for() on all elements of img folder(sorted maybe...) 
			images[0] = ImageIO.read(new File("img/freud.jpg"));
			images[1] = ImageIO.read(new File("img/schopenhauer.jpg"));
			images[2] = ImageIO.read(new File("img/kierkegaard.jpg"));
			images[3] = ImageIO.read(new File("img/platone.jpg"));
			images[4] = ImageIO.read(new File("img/hobbes.jpg"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//this method is used to redraw every time elements on the gui
	@Override
	public void paint(Graphics g) {
		/*for every elements  i do the same check,
		this think let me lose blood from the eyes, in future i will correct this with 1 for
		and store measures in another class*/
		//used to clear strings and recreate(strings do not have the same value for gui as the filled things)
		super.paintComponents(g);
		//set color in base of status
		if(status[0]) {
			g.setColor(Color.red);
		}
		else {
			g.setColor(Color.GREEN);
		}
		//create oval in a specified position
		g.fillOval(400, 475, 100, 100);
		//write string
		g.setColor(Color.BLACK);
		g.drawString("Freud", 435, 475);
		//draw an image
		g.drawImage(images[0], 400, 580, 100, 100, null);
		//draw last up(eat), for check mathemathic relation down <= up <= down+1
		g.drawString("up(eat): " + String.valueOf(Info.getUp(0)), 520, 600);
		//draw last date of up
		g.drawString("Date of the last eat: " + Info.getDate(0), 520, 620);
		
		if(status[1]) {
			g.setColor(Color.red);
		}
		else {
			g.setColor(Color.GREEN);
		}
		g.fillOval(500, 400, 100, 100);
		g.setColor(Color.BLACK);
		g.drawString("Schopenhauer", 535, 400);
		g.drawImage(images[1], 650, 380, 75, 100, null);
		g.drawString("up(eat): " + String.valueOf(Info.getUp(1)), 750, 400);
		g.drawString("Date of the last eat: " + Info.getDate(1), 750, 420);
		
		if(status[2]) {
			g.setColor(Color.red);
		}
		else {
			g.setColor(Color.GREEN);
		}
		g.fillOval(450, 300, 100, 100);
		g.setColor(Color.BLACK);
		g.drawString("Kierkegaard", 485, 300);
		g.drawImage(images[2], 525, 180, 75, 100, null);
		g.drawString("up(eat): " + String.valueOf(Info.getUp(2)), 630, 200);
		g.drawString("Date of the last eat: " + Info.getDate(2), 630, 220);
		
		if(status[3]) {
			g.setColor(Color.red);
		}
		else {
			g.setColor(Color.GREEN);
		}
		g.fillOval(340, 300, 100, 100);
		g.setColor(Color.BLACK);
		g.drawString("Platone", 375, 300);
		g.drawImage(images[3], 300, 180, 100, 100, null);
		g.drawString("up(eat): " + String.valueOf(Info.getUp(3)), 110, 200);
		g.drawString("Date of the last eat: " + Info.getDate(3), 110, 220);
		
		if(status[4]) {
			g.setColor(Color.red);
		}
		else {
			g.setColor(Color.GREEN);
		}
		g.fillOval(300, 400, 100, 100);
		g.setColor(Color.BLACK);
		g.drawString("Hobbes", 300, 400);
		g.drawImage(images[4], 180, 390, 100, 100, null);
		g.drawString("up(eat): " + String.valueOf(Info.getUp(4)), 10, 500);
		g.drawString("Date of the last eat: " + Info.getDate(4), 10, 520);
		
	}
	//set status of thread,according to this i will go the if's statements 
	public void setStatus(Integer n,Boolean value) {
		this.status[n] = value;
	}
}
