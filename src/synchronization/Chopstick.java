package synchronization;

/**
 * @author giordano colli
 * @value this class is used to modify and check if a chopstick is usable 
 */
public class Chopstick {
	//this is the value of chopstick
	private Boolean value;
	
	//constructor is used to assing initial value to chopstick
	public Chopstick(Boolean value) {
		this.value = value;
	}
	
	//these 2 synchronized methods are used to take and leave chopstick
	//synchronized will certificate 1 access per time from threads
	public synchronized void take() {
		while(!this.value) { //if it's not possible to take chopstick thread will wait until another thread use the leave method
			try {wait();} catch (InterruptedException e){e.printStackTrace();}
		}
		this.value = false; //if it's possible to take chopstick thread will set it value to false(will take it)
	}
	
	//used to set chopstick as available
	public synchronized void leave() {
		this.value = true;
		notify();
	}
	
}
